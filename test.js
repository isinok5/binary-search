describe('Binary Search', function() {

    describe('Ищет элемент в массиве, используя бинарный поиск', function() {

        let arr = [3, 345, 102, 5000, 4, 1, 35, 3543, 2, 0];
        function makeTest(item) {
            it('Поиск ' + item + ' в массиве ' + arr , function() {
                assert.equal(binarySearch(arr, item), true);
            });
        } 
        for (let i = 0; i <= 4; i ++) {
            makeTest(i);
        } 
    });

    it('Если число не найдено, функция возвращает -1', function() {
        assert.equal(binarySearch([1,4,5,1,5], 1001), -1);
    });

    it('При передачи пустого массива в функцию, она возращает null', function() {
        assert.equal(binarySearch([], 1), null);
    });

    it('При передачи значения отличного от массива в функцию, она возвращает null', function() {
        assert.equal(binarySearch({ a: 2 }, 1), null);
    });

    it('Если второй аргумент функции не число, она вернет NaN', function() {
        assert(isNaN(binarySearch([1,4,5,1,5], 'dsfasgd')));
    });

});