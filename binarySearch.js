'use strict'

let arr = [3, 345, 102, 5000, 4, 1, 35, 3543, 2, 0];

console.log( binarySearch(arr, 0) );

function binarySearch(arr, item) {
  //Проверка массива на пустоту и на то, что является ли данная сущность массивом
  if (!arr.length || !Array.isArray(arr)) return null;

  //Проверка на число значения, которое нужно найти
  var isNumeric = function(item) {
    return !isNaN(parseFloat(item)) && isFinite(item);
  }

  //Проверка на число каждого значения в массиве
  var arrNaN = arr.every(function isNanInArr(value) {
    return isNumeric(value);
  });

  if (!arrNaN || !isNumeric(item)) return NaN; 

  //Функция для сравнения чисел в массиве
  function compareNumbers(a, b) {
    return a - b;
  }
  //Сортировка массива
  arr.sort(compareNumbers);

  let left = 0;
  let right = arr.length - 1;
  
  //Алгоритм бинарного поиска
  while (right >= left) {
    let mid = Math.floor((left + right) / 2);

    if (arr[mid] === item) return true;
    arr[mid] < item ? left = mid + 1 : right = mid - 1;
  }

  return -1;
}